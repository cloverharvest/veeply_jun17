// This is a basic configuration file for Karma.
// This tells karma what testing library we are using,
// where our JS tests are located,
// what browser to use to execute them.
// We also need to point karma-webpack at our Webpack config so it
// can use Webpack to process the files.

module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],
    files: [
      '**/*.spec.js'
    ],
    preprocessors: {
      '**/*.spec.js': ['webpack']
    },
    webpack: require('../../config/webpack/test.js'),
    browsers: ['PhantomJS'] });
};
