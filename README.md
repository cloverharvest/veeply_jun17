# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

• Ruby 2.4.1 -ok 
• Rails 5.1.1 - ok 
• Bootstrap 3.3.7 - ok
• Angular 4.1.3

• Postgres 9.6. 3 - ok
 • Webpack 2.6.1 - ok
• Node 8.1 - ok 
• Devise 4.3.0  - ok
• Karma 1.7.0

* System dependencies

1) If there is a file permission problem as an `admin`, like `Warning: You have unlinked kegs in your Cellar
Leaving kegs unlinked can lead to build-trouble and cause brews that depend on
those kegs to fail to run properly once built. Run `brew link` on these:
  node`

  Use this command to allow permission: `sudo chown -R $USER /usr/local`
  Node via yarn homebrew is now installed correctly.  

* Configuration

1) Use Foreman to allow us to run a number of commands to run at the same time and in the same window.

So that the web-pack-dev-server (serves up webpack managed code aka javascript and css code)
and the rails server (serves up backend code)

List all the commands we want to run in the `Procfile`. Create this file in root.

Invoke with `foreman start`

2) Yarn is a JavaScript package manager. Think `yarn install` instead of `bundle install`.

* Database creation
1) Use postgresql to run up the db
2) Use postico for db visualization

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* Other notes:

1) The javascript_pack_tag and stylesheet_pack_tag are helpers provided by Webpacker gem that creates a .css that we can include in our views

We declare this in the head of our `application.html.erb`.  

2) Use `rails db` or `rails dbconsole` to access the Postgres Database. You can
run your SQL commands here.
Some commands = `\x` for expanded display
              = `select * from MODEL(s);`
              = `\q` then Hit Return to close the console

3) PW: changemetoo
